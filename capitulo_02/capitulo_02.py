# coding: utf-8
# DATE: 08/10/2019
# END DATE: 15/10/2019

# Estruturas de Dados do Pandas

import pandas as pd
from os import path

pdfe = "/home/kylefelipe/estudo_pandas/pandas_for_everyone/data/"
root = "/home/kylefelipe/estudo_pandas/livro_analisedados_pandas/"

s = pd.Series(['Banana', '42'])

# Atribuindo nome a um indice manualmente em uma série
# Passando um indice

s = pd.Series(['Wes MacKinsey', 'Creator of Pandas'], index=['person', 'who'])

scientists = pd.DataFrame(
    data={"occupation": ['Chemist', 'Statistician'],
          "born": ['1920-07-25', '1876-06-13'],
          "died": ['1958-04-16', '1927-10-16'],
          "age": [37, 61]},
    index=['Rosaline Franklin', 'Willem Gosset'], 
    columns=['occupation', 'born', 'died', 'age'])

# Dicionários padrões do python não são ordenados
# para usar um dicionario ordenado basta usar o OrderedDict do collections

from collections import OrderedDict

scientists = pd.DataFrame(OrderedDict([
    ("name", ['Rosaline Franklin', 'Willem Gosset']),
    ("occupation", ['Chemist', 'Statistician']),
    ("born", ['1920-07-25', '1876-06-13']),
    ("died", ['1958-04-16', '1927-10-16']),
    ("age", [37, 61])
    ])
)

# Séries

scientists = pd.DataFrame(
    data={"occupation": ['Chemist', 'Statistician'],
        "born": ['1920-07-25', '1876-06-13'],
        "died": ['1958-04-16', '1927-10-16'],
        "age": [37, 61]},
    index=['Rosaline Franklin', 'Willem Gosset'], 
    columns=['occupation', 'born', 'died', 'age'])

first_row = scientists.loc['Willem Gosset']
print(type(first_row))
print(first_row.index)
print(first_row.values)
print(first_row.keys())
print(first_row.keys()[0])

ages = scientists['age']
ages.mean()
ages.min()
ages.max()
ages.std()

# Subconjuntos com booleanos: series
scientists = pd.read_csv(path.join(pdfe, 'scientists.csv'))
ages = scientists['Age']
ages.describe()
ages.mean()
ages[ages > ages.mean()]
type(ages > ages.mean())
manual_bool_values = [True, True, False, False, True, True, False, True]
ages[manual_bool_values]
ages + ages
ages * ages

ages + 2

# Vetores com tamanhos diferentes

ages + pd.Series([1, 100])

import numpy as np

# causa erro

ages +  np.array([1, 100])

# Rótulos de indices comuns

ages
rev_ages = ages.sort_index(ascending=False)
ages + rev_ages

# DATAFRAME

scientists[scientists["Age"] > scientists["Age"].mean()]

# No panda 0.25,1 esse comando dá erro
scientists.loc[[True, True, False, True]]


# Broadcasting

first_half = scientists[:4]

second_half = scientists[4:]

scientists * 2

# Alteração em Séries e DataFrames
scientists['Born'].dtype, scientists['Died'].dtype
born_datetime = pd.to_datetime(scientists['Born'], format='%Y-%m-%d')
died_datetime = pd.to_datetime(scientists['Died'], format='%Y-%m-%d')

scientists['born_dt'], scientists['died_dt'] = (born_datetime, died_datetime)
scientists.head()
scientists.shape

scientists['Age']

import random

random.seed(42)
random.shuffle(scientists['Age'])
scientists['Age'] = scientists['Age'].\
    sample(len(scientists['Age']), random_state=24).\
        reset_index(drop=True)

# DATAS

scientists['age_days_dt'] = (scientists['died_dt'] - \
                             scientists['born_dt'])
scientists['age_days_dt'] = scientists['age_days_dt'].astype('timedelta64[Y]')

# Descartando dados
scientists.columns
scientists_dropped = scientists.drop(['Age'], axis=1)

# Exportando dados

## Pickle

names = scientists['Name']

output = path.join(root, 'output/')

def gera_path(nome) -> str:
    """Gera o path do arquivo pickle"""
    caminho = path.join(output, nome) + ".pickle"
    return caminho

names.to_pickle(gera_path('scientists_names_series'))

scientists_name_from_pickle = pd.read_pickle(gera_path("scientists_names_series"))

scientists.to_pickle(gera_path("scientist_df"))

scientists_from_pickle = pd.read_pickle(gera_path("scientist_df"))

## CSV

def gera_path_sv(nome) -> str:
    """Gera o path do arquivo pickle"""
    caminho = path.join(output, nome)
    return caminho

names.to_csv(gera_path_sv("scientists_names_series")+".csv")
scientists.to_csv(gera_path_sv('scientists_df')+'.ssv', sep=';', index=False)

## EXCEL

names_df = names.to_frame()
import xlwt # Para manipular arquivos EXCEL

names.to_excel(gera_path_sv('scientists_names_series_df.xls'))

import openpyxl
names.to_excel(gera_path_sv('scientists_names_series_df.xlsx'))

## Exportando o dado para uma planilha expecífica
names.to_excel(gera_path_sv('scientists_names_series_df_2.xlsx'),
               sheet_name='scientists',
               index=False)
