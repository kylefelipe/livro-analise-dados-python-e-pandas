# coding: utf-8
# DATE: 05/10/2019
# END DATE: 08/10/2019

import pandas as pd
import matplotlib.pyplot as plt
from os import path

pdfe = "/home/kylefelipe/estudo_pandas/pandas_for_everyone/data/"
gapminder = path.join(pdfe,"gapminder.tsv")
df = pd.read_csv(gapminder, sep='\t')


# Mostrando as 5 pirmeiras linhas
print(df.head())

# Mostrando o tipo de dado
print(type(df))

# Mostrando a quantidade de linhas e colunas
print(df.shape)

# Mostrando os nomes das colunas
print(df.columns)

# Mostrando os tipos de cada coluna
print(df.dtypes)

# Mostrando mais informações sobre os dados
print(df.info())

# Para acessar sub-conjunto de dados de coluna pelo nome

country_df = df['country']
print(country_df.head())
print(country_df.tail())

# Acessando um subconjunto
subset = df[['country', 'continent', 'year']]

print(subset.head())
print(subset.tail())

# Acessano a linha pelo indice - loc
print(df.loc[0])
print(df.loc[99])
print(df.loc[-1]) # Deveria imprimir a ultima linha do conjunto de dados

# Pegando a ultima linha
number_of_rows = df.shape[0]
last_row_index = number_of_rows - 1

print(df.loc[last_row_index])

# Pegando as linhas de trás para frente - it works
def backwars(neg):
    lri = df.shape[0] + neg
    return lri

print(df.loc[backwars(-1)])

print(df.tail(n=1))

subset_loc = df.loc[0]
subset_head = df.head(n=1)

print(type(subset_loc))
print(type(subset_head))

# Exibindo conjunto de linas

print(df.loc[[0, 99, 999]])

print(df.iloc[1])
print(df.iloc[-1])
print(df.iloc[[0, 99, 999]])
df.ix[0]

subset2 = df.loc[:, ['country', 'year', 'pop']]
print(subset2.head())

subset3 = df.iloc[:, [2, 4, -1]]
print(subset3.head())

small_range = list(range(5))

subset4 = df.iloc[:, small_range]
subset4.head()

# Fatiou, fatiou, fatiou
subset = df.iloc[:, ::2]
subset.head()

df.loc[10:13, ['country', 'lifeExp', 'gdpPercap']]
print(df.loc[10:13, ['country', 'lifeExp', 'gdpPercap']])

# Calculos agrupados e agregados

print(df.head(n=10))

# Médias agrupadas

df.groupby('year')['lifeExp'].mean()

grouped_year_df = df.groupby('year')

grouped_year_df_lifeExp = grouped_year_df['lifeExp']
mean_lifeExp_by_year = grouped_year_df_lifeExp.mean()

multi_group_var = df.\
    groupby(['year', 'continent'])\
        [['lifeExp', 'gdpPercap']].mean()

flat = multi_group_var.reset_index()

# A ordem da lista q usamos para agrupar os dados importa?
# sim

print(df.groupby('continent')['country'].value_counts())

# O que vocês obterá se usar value_counts no lugar de nunique?
# quantidade de linhas agrupado por continente e país.

# 1.5 Plotagem Básica

global_yearly_life_expectancy = df.groupby('year')['lifeExp'].mean()
global_yearly_life_expectancy.plot()

# No livro não tem o import do matplotlib, feito acima, com ele é possível plotar o grafico.
plt.show()


