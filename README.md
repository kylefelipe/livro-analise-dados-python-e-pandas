# Análise de dados com Python e Pandas

No intuito de melhorar minhas skills em estatística, continuar a aprender mais sobre python, comprei esse livro em 2018, e após quase um ano de aquisição do mesmo consegui separar um tempo do meu dia a dia para sentar e estudar a biblioteca.  
Esse é o intuito desse material, conter todos os códigos, anotações e idéias que foram surgindo durante o estudo.
Coisas que constam no livro e outras que eu modificar de acordo com a minha imaginação for me dando liberdade de criar.

__Inicio:__ 05/10/2019

## Dicas VSCODE

### ATALHOS

* `SHIFT + ENTER`: roda a linha atual do cursor no terminal iterativo do python.
* `CTRL + ENTER`: cria uma nova linha a baixo do cursor e o move para lá independente de onde ele estiver, sem quebrar a linha atual.
