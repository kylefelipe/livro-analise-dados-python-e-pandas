# coding: utf-8
# DATE: 08/10/2019
# END DATE: 15/10/2019

# Introdução à Plotagem

import pandas as pd
from os import path
import seaborn as sns
import matplotlib.pyplot as plt

pdfe = "/home/kylefelipe/estudo_pandas/pandas_for_everyone/data/"
root = "/home/kylefelipe/estudo_pandas/livro_analisedados_pandas/"
output = "/home/kylefelipe/estudo_pandas/livro_analisedados_pandas/output/"

anscombe = sns.load_dataset("anscombe")

# Criando um subconjunto de dados
# contendo apenas o primeiro conjunto de dados de Anscombe

dataset_1 = anscombe[anscombe["dataset"] == 'I']

plt.plot(dataset_1['x'], dataset_1['y'])
plt.show()

# Plotando pontos

plt.plot(dataset_1['x'], dataset_1['y'], 'o')
plt.show()

dataset_2 = anscombe[anscombe['dataset'] == 'II']
dataset_3 = anscombe[anscombe['dataset'] == 'III']
dataset_4 = anscombe[anscombe['dataset'] == 'IV']

# Subplots
# Criando as imagens

fig = plt.figure()

axes1 = fig.add_subplot(2, 2, 1)
axes2 = fig.add_subplot(2, 2, 2)
axes3 = fig.add_subplot(2, 2, 3)
axes4 = fig.add_subplot(2, 2, 4)

axes1.plot(dataset_1['x'], dataset_1['y'], 'o')
axes2.plot(dataset_2['x'], dataset_2['y'], 'o')
axes3.plot(dataset_3['x'], dataset_3['y'], 'o')
axes4.plot(dataset_4['x'], dataset_4['y'], 'o')

# Acrescentando titulos aos datasets
axes1.set_title('Dataset 1')
axes2.set_title('Dataset 2')
axes3.set_title('Dataset 3')
axes4.set_title('Dataset 4')

# Adicionando titulo para a figura toda
fig.suptitle("Anscombe Data") 

# Organizando layout
fig.tight_layout()

fig.show()

# Graficos estatisticos
# Univariados
## Histogramas

tips = sns.load_dataset('tips')
tips.head()
fig = plt.figure()
axes1 = fig.add_subplot(1, 1, 1)
axes1.hist(tips['total_bill'], bins=10)
axes1.set_title('Histogram of Total Bill')
axes1.set_xlabel('Frequency')
axes1.set_ylabel('Total Bill')
fig.show()

# Bivariados
## Gráficos de Dispersão

scatter_plot =  plt.figure()
axes1 = scatter_plot.add_subplot(1, 1, 1)
axes1.scatter(tips['total_bill'], tips['tip'])
axes1.set_title('Scatterplot of Total Bill vs Tip')
axes1.set_xlabel('Total Bill')
axes1.set_ylabel('Tip')
scatter_plot.show()

## Gráfico de caixa

boxplot = plt.figure()
axes1 = boxplot.add_subplot(1, 1, 1)
axes1.boxplot([tips[tips['sex'] == 'Female']['tip'],
              tips[tips['sex'] == 'Male']['tip']],
              labels=['Female', 'Male'])
axes1.set_xlabel('Sex')
axes1.set_ylabel('Tip')
axes1.set_title('Boxplot of Tips by Sex')
boxplot.show()

# Multivariados


def recode_sex(sex):
    if sex == 'Female':
        return 0
    else:
        return 1

tips['sex_color'] = tips['sex'].apply(recode_sex)
scatter_plot = plt.figure()
axes1 = scatter_plot.add_subplot(1, 1, 1)
axes1.scatter(x=tips['total_bill'],
              y=tips['tip'],
              s=tips['size'] * 10,
              c=tips['sex_color'],
              alpha=0.5)
axes1.set_title('Total Bill vs Tip Colored by Sex and Sized by Size')
axes1.set_xlabel('Total Bill')
axes1.set_ylabel('Tip')
scatter_plot.show()

# SEABORN
## Univariado
### Histogramas

hist, ax = plt.subplots()

ax = sns.distplot(tips['total_bill'])
ax.set_title('Total Bill Histogram with Density Plot')

plt.show()

# Plotando apenas o histograma

hist, ax = plt.subplots()

ax = sns.distplot(tips['total_bill'], kde=False)
ax.set_title('Total Bill Histogram with Density Plot')
ax.set_xlabel('Total Bill')
ax.set_ylabel('Frequency')

plt.show()

# Apenas o kernel

hist, ax = plt.subplots()

ax = sns.kdeplot(tips['total_bill'])
ax.set_title('Total Bill Histogram with Density Plot')
ax.set_xlabel('Total Bill')
ax.set_ylabel('Frequency')

plt.show()

### Rugplot - grafico de tapete

hist_den_rug, ax = plt.subplots()
ax = sns.distplot(tips['total_bill'], rug=True)
ax.set_title('Total Bill Histogram with Density and Rug Plot')
ax.set_xlabel('Total Bill')
plt.show()

### Barras

count, ax = plt.subplots()
ax = sns.countplot('day', data=tips)
ax.set_title('Count of Days')
ax.set_xlabel('Day of the Week')
ax.set_ylabel('Frequency')
plt.show()
